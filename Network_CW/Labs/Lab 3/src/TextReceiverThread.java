/*
 * TextReceiver.java
 */

/**
 *
 * @author  abj
 */
import java.math.BigInteger;
import java.net.*;
import java.io.*;
import java.nio.ByteBuffer;
import java.text.NumberFormat;
import java.text.ParseException;

public class TextReceiverThread implements Runnable{
    
    static DatagramSocket receiving_socket;
    
    public void start(){
        Thread thread = new Thread(this);
	thread.start();
    }
    
    public void run (){
     
        //***************************************************
        //Port to open socket on
        int PORT = 55555;
        //***************************************************
        
        //***************************************************
        //Open a socket to receive from on port PORT
        
        //DatagramSocket receiving_socket;
        try{
		receiving_socket = new DatagramSocket(PORT);
	} catch (SocketException e){
                System.out.println("ERROR: TextReceiver: Could not open UDP socket to receive from.");
		e.printStackTrace();
                System.exit(0);
	}
        //***************************************************
        
        //***************************************************
        //Main loop.
        
        boolean running = true;
        while (running){
         
            try{
                byte[] buffer = new byte[4];
                DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
                receiving_socket.receive(packet);
                NumberFormat numberFormat = NumberFormat.getInstance();
                numberFormat.setParseIntegerOnly(true);
                buffer = new byte[numberFormat.parse(new String(buffer)).intValue()];
                packet = new DatagramPacket(buffer, buffer.length);
                receiving_socket.receive(packet);
                //Get a string from the byte buffer
                String str = new String(buffer);
                //Display it
                System.out.println(str);
                
                //The user can type EXIT to quit
                if (str.substring(0,4).equals("EXIT")){
                     running=false;
                }
            } catch (IOException e){
                System.out.println("ERROR: TextReceiver: Some random IO error occured!");
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        //Close the socket
        receiving_socket.close();
        //***************************************************
    }

    public static final byte[] intToByteArray(int value) {
        return new byte[] {
                (byte)(value >>> 24),
                (byte)(value >>> 16),
                (byte)(value >>> 8),
                (byte)value};
    }
}
