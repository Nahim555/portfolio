package Logging;

public class Logger {

    public Logger() {

    }

    protected void log(String msg) {
        System.out.println(getClass().getSimpleName() + hashCode() + ": " + msg);
    }

}
