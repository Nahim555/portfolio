package Voip.Payload;

import java.io.IOException;

public interface PayloadProducer {
    byte[] produce() throws IOException;
}
