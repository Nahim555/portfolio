package Voip.Payload;

import java.io.IOException;

public interface PayloadHandler {
    void handle(byte[] payload) throws IOException;
}
