package Voip.Payload;

import java.io.IOException;

public interface PayloadConsumer {
    void consume(byte[] payload) throws IOException;
}
