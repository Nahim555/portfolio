package Voip.Payload;

import java.io.IOException;

public class MultiLayer implements Layer {

    private final Layer[] layers;

    public MultiLayer(Layer... layers) {
        this.layers = layers;
    }

    @Override
    public void processPayload(byte[] payload, PayloadHandler payloadHandler) throws IOException {
        sendPayload(0, payload, payloadHandler);
    }

    @Override
    public int getPacketSizeChange(int payloadSize) {
        int size = payloadSize;
        for (Layer layer : layers) {
            size = layer.getPacketSizeChange(size);
        }
        return size;
    }

    @Override
    public int getTimeout(int timeout) {
        for (Layer layer : layers) {
            timeout = layer.getTimeout(timeout);
        }
        return timeout;
    }


    private void sendPayload(int index, byte[] payload, PayloadHandler payloadHandler) throws IOException {
        if (index == layers.length) {
            payloadHandler.handle(payload);
        } else {
            PayloadHandler handler = new PayloadHandler() {
                @Override
                public void handle(byte[] payload) throws IOException {
                    sendPayload(index + 1, payload, payloadHandler);
                }
            };
            layers[index].processPayload(payload, handler);
        }
    }
}
