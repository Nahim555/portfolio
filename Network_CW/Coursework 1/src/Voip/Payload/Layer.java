package Voip.Payload;

import java.io.IOException;

public interface Layer {
    void processPayload(byte[] payload, PayloadHandler payloadHandler) throws IOException;

    int getPacketSizeChange(int payloadSize);

    int getTimeout(int timeout);
}
