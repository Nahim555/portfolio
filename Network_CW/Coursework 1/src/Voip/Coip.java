package Voip;

import Voip.Communication.Receiver;
import Voip.Communication.Sender;
import Voip.Communication.Stoppable;
import Voip.Filling.Filler;
import Voip.Payload.MultiLayer;

import javax.sound.sampled.LineUnavailableException;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public abstract class Coip extends Stoppable {
    private final Sender sender;
    private final Receiver receiver;

    public Coip(Configuration option, int ipA, int ipB, int ipC, int ipD, int port)
            throws IOException, LineUnavailableException {
        sender = getSender(option.obtainer.getSocket(), option.sendLayer,
                InetAddress.getByAddress(new byte[]{(byte) ipA, (byte) ipB, (byte) ipC, (byte) ipD}),
                port, getPacketSize());
        receiver = getReceiver(option.obtainer.getSocket(port), option.receiveLayer, option.filler,
                getPacketSize());
        sender.start();
//        try {
//            Thread.sleep(20000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        receiver.start();
    }

    @Override
    public void onStop() {
        sender.stop();
        receiver.stop();
    }

    protected abstract int getPacketSize();

    protected abstract Sender getSender(DatagramSocket socket, MultiLayer layer, InetAddress destination, int port,
                                        int packetSize) throws LineUnavailableException;

    protected abstract Receiver getReceiver(DatagramSocket socket, MultiLayer layer, Filler filler, int packetSize) throws LineUnavailableException, SocketException;
}
