package Voip;

import Voip.Communication.AudioReceiver;
import Voip.Communication.AudioSender;
import Voip.Communication.Receiver;
import Voip.Communication.Sender;
import Voip.Filling.Filler;
import Voip.Payload.MultiLayer;

import javax.sound.sampled.LineUnavailableException;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class Voip extends Coip {

    public Voip(Configuration option, int ipA, int ipB, int ipC, int ipD, int port) throws IOException, LineUnavailableException {
        super(option, ipA, ipB, ipC, ipD, port);
    }

    @Override
    protected int getPacketSize() {
        return 512;
    }

    @Override
    protected Sender getSender(DatagramSocket socket, MultiLayer layer, InetAddress destination, int port, int packetSize) throws LineUnavailableException {
        return new AudioSender(socket, layer, destination, port, packetSize);
    }

    @Override
    protected Receiver getReceiver(DatagramSocket socket, MultiLayer layer, Filler filler, int packetSize) throws LineUnavailableException, SocketException {
        socket.setSoTimeout(layer.getTimeout(32));
        return new AudioReceiver(socket, layer, filler, packetSize);
    }
}
