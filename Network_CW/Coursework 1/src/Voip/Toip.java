package Voip;

import Voip.Communication.Receiver;
import Voip.Communication.Sender;
import Voip.Communication.TextReceiver;
import Voip.Communication.TextSender;
import Voip.Filling.Filler;
import Voip.Payload.MultiLayer;

import javax.sound.sampled.LineUnavailableException;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class Toip extends Coip {

    public Toip(Configuration option, int ipA, int ipB, int ipC, int ipD, int port) throws IOException, LineUnavailableException {
        super(option, ipA, ipB, ipC, ipD, port);
    }

    @Override
    protected int getPacketSize() {
        return 8;
    }

    @Override
    protected Sender getSender(DatagramSocket socket, MultiLayer layer, InetAddress destination, int port, int packetSize) {
        return new TextSender(socket, layer, destination, port, packetSize);
    }

    @Override
    protected Receiver getReceiver(DatagramSocket socket, MultiLayer layer, Filler filler, int packetSize) throws SocketException {
        socket.setSoTimeout(5000);
        return new TextReceiver(socket, layer, filler, packetSize);
    }
}
