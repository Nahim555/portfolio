package Voip.Interleaving;

import Voip.Payload.Layer;
import Voip.Payload.PayloadHandler;

import java.io.IOException;
import java.util.ArrayList;

public class Deinterleaver extends Interleaved implements Layer {

    private ArrayList<byte[]> buffer = new ArrayList<>();

    public Deinterleaver(int dimension) {
        super(dimension);
        index = (byte) (dimension * dimension - 1);
        for (int i = 0; i < dimension * dimension; i++) {
            buffer.add(null);
        }
    }

    @Override
    public void processPayload(byte[] payload, PayloadHandler payloadHandler) throws IOException {
        if (payload != null) {
            byte seq = payload[0];
            byte[] temp = payload;
            payload = new byte[temp.length];
            System.arraycopy(temp, 0, payload, 0, temp.length);
            for (int i = buffer.size(); i < dimension * dimension + seq + index; i++) {
                buffer.add(null);
            }
            buffer.set(dimension * dimension - 1 + seq + index, payload);
        }
        index--;
        if (index < 0) {
            index = (byte) (dimension * dimension - 1);
        }
        for (int i = 0; i < buffer.size(); i++) {
            if (buffer.get(i) == null) {
                System.out.print("n ");
            } else {
                System.out.print(buffer.get(i)[0] + " ");
            }
            if (i == index + 1) {
                System.out.print('|');
            }
        }
        System.out.println();
        byte[] temp = buffer.remove(0);
        if (temp == null) {
            payload = null;
        } else {
            payload = new byte[temp.length - 1];
            System.arraycopy(temp, 1, payload, 0, payload.length);
        }
        payloadHandler.handle(payload);
    }

    @Override
    public int getTimeout(int timeout) {
        return timeout;
    }
}
