package Voip.Interleaving;


import Voip.Payload.Layer;
import Voip.Payload.PayloadHandler;

import java.io.IOException;

public class Interleaver extends Interleaved implements Layer {

    public Interleaver(int dimension) {
        super(dimension);
    }

    @Override
    public void processPayload(byte[] payload, PayloadHandler payloadHandler) throws IOException {
        if (payload != null) {
            byte[] temp = payload;
            payload = new byte[temp.length + 1];
            System.arraycopy(temp, 0, payload, 1, temp.length);
            payload[0] = index;
//            System.out.println("Interleaved: " + index);
        } else {
//            System.out.println("Interleaved: null");
        }
        secondaryBlock[dimension * dimension - rotateIndex() - 1] = payload;
        byte[] output = primaryBlock[index];
        incrementIndex();
        payloadHandler.handle(output);
    }

    @Override
    public int getTimeout(int timeout) {
        return timeout;
    }
}
