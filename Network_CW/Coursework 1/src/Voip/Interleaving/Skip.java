package Voip.Interleaving;

import Voip.Payload.Layer;
import Voip.Payload.PayloadHandler;

import java.io.IOException;

public class Skip implements Layer {

    private final int skip;
    private int count = 0;

    public Skip(int skip) {
        this.skip = skip;
    }

    @Override
    public void processPayload(byte[] payload, PayloadHandler payloadHandler) throws IOException {
        if (count == skip) {
            payloadHandler.handle(payload);
        } else {
            count++;
        }
    }

    @Override
    public int getPacketSizeChange(int payloadSize) {
        return payloadSize;
    }

    @Override
    public int getTimeout(int timeout) {
        return timeout;
    }
}
