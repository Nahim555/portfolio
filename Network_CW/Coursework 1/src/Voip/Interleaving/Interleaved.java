package Voip.Interleaving;

import Logging.Logger;
import Voip.Payload.Layer;

public abstract class Interleaved extends Logger implements Layer {

    protected final int dimension;
    protected byte[][] primaryBlock = null;
    protected byte[][] secondaryBlock;
    protected byte index = 0;

    public Interleaved(int dimension) {
        if (dimension < 1) {
            throw new IllegalArgumentException("Interleaved dimension cannot be less than 1: " + dimension);
        }
        this.dimension = dimension;
        primaryBlock = new byte[dimension * dimension][];
        secondaryBlock = new byte[dimension * dimension][];
    }

    @Override
    public int getPacketSizeChange(int payloadSize) {
        return payloadSize + 1;
    }

    protected static void putByteArrayAtIndex(byte[][] src, int index, byte[] array) {
        if (array == null) {
            src[index] = null;
        } else {
            src[index] = new byte[array.length];
            System.arraycopy(array, 0, src[index], 0, array.length);
        }
    }

    protected byte rotateIndex() {
        byte rotatedIndex = (byte) ((index % dimension) * dimension + dimension - 1 -
                index / dimension);
        return rotatedIndex;
    }

    protected byte rotateIndex(byte index) {
        byte rotatedIndex = (byte) ((index % dimension) * dimension + dimension - 1 -
                index / dimension);
        return rotatedIndex;
    }

    protected void incrementIndex() {
        index++;
        if (index == secondaryBlock.length) {
            primaryBlock = secondaryBlock;
            secondaryBlock = new byte[primaryBlock.length][];
            index = 0;
        }
    }
}
