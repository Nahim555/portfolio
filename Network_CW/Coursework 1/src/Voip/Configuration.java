package Voip;

import Voip.CRC.CrcDecoder;
import Voip.CRC.CrcEncoder;
import Voip.Filling.Filler;
import Voip.Filling.HalvingRepeater;
import Voip.Filling.SilenceFiller;
import Voip.Payload.MultiLayer;
import Voip.Sequencing.InterleaveDesequencer;
import Voip.Sequencing.InterleaveSequencer;
import Voip.Sequencing.Packer;
import Voip.Sequencing.Unpacker;
import uk.ac.uea.cmp.voip.DatagramSocket2;
import uk.ac.uea.cmp.voip.DatagramSocket3;
import uk.ac.uea.cmp.voip.DatagramSocket4;

import java.net.DatagramSocket;
import java.net.SocketException;

public enum Configuration {
    DS1(new MultiLayer(new Packer(3)),
            new MultiLayer(new Unpacker(3)),
            new SocketObtainer() {
                @Override
                public DatagramSocket getSocket() throws SocketException {
                    return new DatagramSocket();
                }

                @Override
                public DatagramSocket getSocket(int port) throws SocketException {
                    return new DatagramSocket(port);
                }
            }, new HalvingRepeater()),
    DS2(new MultiLayer(new InterleaveSequencer(3)),
            new MultiLayer(new InterleaveDesequencer(3)),
            new SocketObtainer() {
                @Override
                public DatagramSocket getSocket() throws SocketException {
                    return new DatagramSocket2();
                }

                @Override
                public DatagramSocket getSocket(int port) throws SocketException {
                    return new DatagramSocket2(port);
                }
            }, new HalvingRepeater()),
    DS3(new MultiLayer(new InterleaveSequencer(3)),
            new MultiLayer(new InterleaveDesequencer(3)),
            new SocketObtainer() {
                @Override
                public DatagramSocket getSocket() throws SocketException {
                    return new DatagramSocket3();
                }

                @Override
                public DatagramSocket getSocket(int port) throws SocketException {
                    return new DatagramSocket3(port);
                }
            }, new HalvingRepeater()),
    DS4(new MultiLayer(new InterleaveSequencer(2), new CrcEncoder()),
            new MultiLayer(new CrcDecoder(), new InterleaveDesequencer(2)),
            new SocketObtainer() {
                @Override
                public DatagramSocket getSocket() throws SocketException {
                    return new DatagramSocket4();
                }

                @Override
                public DatagramSocket getSocket(int port) throws SocketException {
                    return new DatagramSocket4(port);
                }
            }, new HalvingRepeater()),
    DS1_DEFAULT(new MultiLayer(),
            new MultiLayer(),
            new SocketObtainer() {
                @Override
                public DatagramSocket getSocket() throws SocketException {
                    return new DatagramSocket();
                }

                @Override
                public DatagramSocket getSocket(int port) throws SocketException {
                    return new DatagramSocket(port);
                }
            }, new SilenceFiller()),
    DS2_DEFAULT(new MultiLayer(),
            new MultiLayer(),
            new SocketObtainer() {
                @Override
                public DatagramSocket getSocket() throws SocketException {
                    return new DatagramSocket2();
                }

                @Override
                public DatagramSocket getSocket(int port) throws SocketException {
                    return new DatagramSocket2(port);
                }
            }, new SilenceFiller()),
    DS3_DEFAULT(new MultiLayer(),
            new MultiLayer(),
            new SocketObtainer() {
                @Override
                public DatagramSocket getSocket() throws SocketException {
                    return new DatagramSocket3();
                }

                @Override
                public DatagramSocket getSocket(int port) throws SocketException {
                    return new DatagramSocket3(port);
                }
            }, new SilenceFiller()),
    DS4_DEFAULT(new MultiLayer(),
            new MultiLayer(),
            new SocketObtainer() {
                @Override
                public DatagramSocket getSocket() throws SocketException {
                    return new DatagramSocket4();
                }

                @Override
                public DatagramSocket getSocket(int port) throws SocketException {
                    return new DatagramSocket4(port);
                }
            }, new SilenceFiller());

    public final MultiLayer sendLayer;
    public final MultiLayer receiveLayer;
    public final SocketObtainer obtainer;
    public final Filler filler;

    Configuration(MultiLayer layer, MultiLayer receiveLayer, SocketObtainer obtainer, Filler filler) {
        this.sendLayer = layer;
        this.receiveLayer = receiveLayer;
        this.obtainer = obtainer;
        this.filler = filler;
    }

    public String toString() {
        return name();
    }

    public interface SocketObtainer {
        DatagramSocket getSocket() throws SocketException;

        DatagramSocket getSocket(int port) throws SocketException;
    }
}