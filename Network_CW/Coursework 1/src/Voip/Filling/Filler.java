package Voip.Filling;

import Logging.Logger;

public abstract class Filler extends Logger {
    public void update(byte[] payload) {
    }

    public abstract byte[] fill(int length);
}
