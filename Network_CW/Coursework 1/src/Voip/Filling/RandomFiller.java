package Voip.Filling;

import java.util.Random;

public class RandomFiller extends Filler {
    private static final Random RANDOM = new Random();

    @Override
    public byte[] fill(int length) {
        byte[] output = new byte[length];
        RANDOM.nextBytes(output);
        return output;
    }
}
