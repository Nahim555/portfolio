package Voip.Filling;

public class SilenceFiller extends Filler {
    @Override
    public byte[] fill(int length) {
        return new byte[length];
    }
}
