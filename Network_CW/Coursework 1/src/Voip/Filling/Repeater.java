package Voip.Filling;

public class Repeater extends Filler {

    protected byte[] mostRecentPayload;

    @Override
    public void update(byte[] payload) {
        mostRecentPayload = payload;
    }

    @Override
    public byte[] fill(int length) {
        if (mostRecentPayload == null) {
            return mostRecentPayload = new byte[length];
        }
        return mostRecentPayload;
    }
}
