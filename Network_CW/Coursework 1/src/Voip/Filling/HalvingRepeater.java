package Voip.Filling;

public class HalvingRepeater extends Repeater {
    @Override
    public byte[] fill(int length) {
        if (mostRecentPayload != null) {
            for (int i = 0; i < mostRecentPayload.length; i++) {
                mostRecentPayload[i] /= 2;
            }
        }
        return super.fill(length);
    }
}
