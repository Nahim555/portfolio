package Voip.Communication;

import Voip.Payload.MultiLayer;
import Voip.Payload.PayloadHandler;
import Voip.Payload.PayloadProducer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Sender extends Communicator {
    private final PayloadProducer producer;

    public Sender(DatagramSocket socket, MultiLayer layer, InetAddress destination, int port, PayloadProducer producer, int packetSize) {
        super(socket, new DatagramPacket(new byte[layer.getPacketSizeChange(packetSize)],
                layer.getPacketSizeChange(packetSize), destination, port), layer);
        this.producer = producer;
    }

    @Override
    protected void loop() throws IOException {
        layer.processPayload(producer.produce(), new PayloadHandler() {
            @Override
            public void handle(byte[] payload) throws IOException {
                if (payload != null) {
                    packet.setData(payload);
//                System.out.println("Sending: " + new String(payload));
                    socket.send(packet);
                }
            }
        });
    }

    @Override
    public void onStop() {
        System.out.println("Sender stopped");
    }
}
