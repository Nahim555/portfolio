package Voip.Communication;

import Voip.Payload.MultiLayer;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

public abstract class Communicator extends Stoppable {
    protected final DatagramPacket packet;
    protected final DatagramSocket socket;
    protected final MultiLayer layer;
    private final Thread thread = new Thread(new Runnable() {
        @Override
        public void run() {
            try {
                while (!isStopped()) {
                    loop();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    });

    public Communicator(DatagramSocket socket, DatagramPacket packet, MultiLayer layer) {
        this.socket = socket;
        this.layer = layer;
        this.packet = packet;
    }

    public void start() {
        thread.start();
    }

    protected abstract void loop() throws IOException;
}
