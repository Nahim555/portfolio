package Voip.Communication;

import Logging.Logger;

import java.util.concurrent.atomic.AtomicBoolean;

public abstract class Stoppable extends Logger {

    private final AtomicBoolean stop = new AtomicBoolean(false);

    public boolean isStopped() {
        boolean stopped = stop.get();
        return stopped;
    }

    public void stop() {
        if (stop.compareAndSet(false, true)) {
            log("Stopping");
            onStop();
        } else {
            log("Already stopped / stopping");
        }
    }

    public abstract void onStop();
}
