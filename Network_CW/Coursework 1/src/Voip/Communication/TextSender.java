package Voip.Communication;

import Voip.Payload.MultiLayer;
import Voip.Payload.PayloadProducer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.DatagramSocket;
import java.net.InetAddress;

import static java.lang.Math.min;

public class TextSender extends Sender {
    public TextSender(DatagramSocket socket, MultiLayer layer, InetAddress destination, int port, int packetSize) {
        super(socket, layer, destination, port, new PayloadProducer() {
            private final BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
            private byte i = 0;

            @Override
            public byte[] produce() throws IOException {
                // TODO handle packet size here, since string could be longer than packet
                byte[] input = bufferedReader.readLine().getBytes();
                byte[] buffer = new byte[packetSize];
                System.arraycopy(input, 0, buffer, 0, min(buffer.length, input.length));
                return buffer;
//                return new byte[]{Byte.parseByte(bufferedReader.readLine())};
//                byte[] buffer = new byte[packetSize];
//                buffer[0] = i;
//                i = (byte) ((i + 1) % 10);
//                return buffer;
            }
        }, packetSize);
    }
}
