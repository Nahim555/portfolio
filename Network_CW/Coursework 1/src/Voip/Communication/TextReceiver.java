package Voip.Communication;

import Voip.Filling.Filler;
import Voip.Payload.MultiLayer;
import Voip.Payload.PayloadConsumer;

import java.net.DatagramSocket;
import java.net.SocketException;

public class TextReceiver extends Receiver {
    public TextReceiver(DatagramSocket socket, MultiLayer layer, Filler filler, int packetSize) throws SocketException {
        super(socket, layer, filler, packetSize, new PayloadConsumer() {
            @Override
            public void consume(byte[] payload) {
                if (payload == null) {
                    System.out.println("Null payload");
                } else {
                    System.out.println("Received: " + new String(payload));
                }
            }
        });
    }
}
