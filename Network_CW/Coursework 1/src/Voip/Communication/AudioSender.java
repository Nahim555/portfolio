package Voip.Communication;

import CMPC3M06.AudioRecorder;
import Voip.Payload.MultiLayer;
import Voip.Payload.PayloadProducer;

import javax.sound.sampled.LineUnavailableException;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class AudioSender extends Sender {

    public AudioSender(DatagramSocket socket, MultiLayer layer, InetAddress destination, int port, int packetSize) throws LineUnavailableException {
        super(socket, layer, destination, port, new PayloadProducer() {
            private final AudioRecorder recorder = new AudioRecorder();

            @Override
            public byte[] produce() throws IOException {
                return recorder.getBlock();
            }
        }, packetSize);
    }
}
