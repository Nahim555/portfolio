package Voip.Communication;

import Voip.Filling.Filler;
import Voip.Payload.MultiLayer;
import Voip.Payload.PayloadConsumer;
import Voip.Payload.PayloadHandler;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;

public class Receiver extends Communicator {
    private final Filler filler;
    private final PayloadConsumer consumer;
    private final int consumerPacketSize;

    public Receiver(DatagramSocket socket, MultiLayer layer, Filler filler, int packetSize, PayloadConsumer consumer) throws SocketException {
        super(socket, new DatagramPacket(new byte[layer.getPacketSizeChange(packetSize)],
                layer.getPacketSizeChange(packetSize)), layer);
        this.filler = filler;
        this.consumerPacketSize = packetSize;
        this.consumer = consumer;
    }

    @Override
    protected void loop() throws IOException {
        byte[] payload;
        try {
            socket.receive(packet);
            payload = packet.getData();
        } catch (SocketTimeoutException e) {
            System.out.println("Loss");
            payload = null;
        }
        layer.processPayload(payload, new PayloadHandler() {
            @Override
            public void handle(byte[] payload) throws IOException {
                if (payload == null) {
                    consumer.consume(filler.fill(consumerPacketSize));
                } else {
                    filler.update(payload);
                    consumer.consume(payload);
                }
            }
        });
    }

    @Override
    public void onStop() {
        System.out.println("Receiver stopped");
    }
}
