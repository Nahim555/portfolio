package Voip.Communication;

import CMPC3M06.AudioPlayer;
import Voip.Filling.Filler;
import Voip.Payload.MultiLayer;
import Voip.Payload.PayloadConsumer;

import javax.sound.sampled.LineUnavailableException;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.SocketException;

// TODO make toip mirror this setup

public class AudioReceiver extends Receiver {
    public AudioReceiver(DatagramSocket socket, MultiLayer layer, Filler filler, int packetSize) throws LineUnavailableException, SocketException {
        super(socket, layer, filler, packetSize, new PayloadConsumer() {
            private final AudioPlayer player = new AudioPlayer();

            @Override
            public void consume(byte[] payload) throws IOException {
                player.playBlock(payload);
                filler.update(payload);
            }
        });
    }
}
