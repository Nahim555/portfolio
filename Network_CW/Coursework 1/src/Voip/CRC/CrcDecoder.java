package Voip.CRC;

import Voip.Payload.Layer;
import Voip.Payload.PayloadHandler;

import java.io.IOException;

import static Voip.CRC.CrcEncoder.crc8;

public class CrcDecoder implements Layer {
    @Override
    public void processPayload(byte[] payload, PayloadHandler payloadHandler) throws IOException {
        if (payload == null) {
            payloadHandler.handle(null);
        } else {
            byte[] temp = new byte[payload.length - 1];
            byte crc = payload[0];
            System.arraycopy(payload, 1, temp, 0, payload.length - 1);
            payload = temp;
            byte recalculatedCrc = crc8(payload);
            if (crc != recalculatedCrc) {
                System.out.println("Corrupt");
                payloadHandler.handle(null);
            } else {
                payloadHandler.handle(payload);
            }
        }
    }

    @Override
    public int getPacketSizeChange(int payloadSize) {
        return payloadSize + 1;
    }

    @Override
    public int getTimeout(int timeout) {
        return timeout;
    }
}
