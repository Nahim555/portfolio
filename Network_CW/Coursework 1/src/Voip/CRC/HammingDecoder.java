package Voip.CRC;

import Voip.Payload.Layer;
import Voip.Payload.PayloadHandler;

import java.io.IOException;

import static Voip.CRC.HammingCode.*;

public class HammingDecoder implements Layer {

    private final byte[][] buffer;
    private int bitIndex = 0;
    private int packetLength;

    public HammingDecoder() {
        buffer = new byte[HammingCode.SIZE][];
    }

    @Override
    public void processPayload(byte[] payload, PayloadHandler payloadHandler) throws IOException {
        if (payload != null) {
            packetLength = payload.length;
            buffer[bitIndex] = new byte[payload.length];
            System.arraycopy(payload, 0, buffer[bitIndex], 0, payload.length);
        }
        bitIndex++;
        if (bitIndex == buffer.length) {
            if (validBuffer()) {
                HammingCode[] codes = new HammingCode[packetLength * Byte.SIZE];
                for (int byteBitIndex = 0; byteBitIndex < Byte.SIZE; byteBitIndex++) {
                    for (int byteIndex = 0; byteIndex < packetLength; byteIndex++) {
                        boolean[] bits = new boolean[SIZE];
                        for (int codeBitIndex = 0; codeBitIndex < bits.length; codeBitIndex++) {
                            bits[codeBitIndex] = getBit(buffer[codeBitIndex][byteIndex], byteBitIndex);
                        }
                        codes[byteIndex * Byte.SIZE + byteBitIndex] = new HammingCode(bits);
                    }
                }
                byte[] bytes = new byte[codes.length / CODES_PER_BYTE];
                for (int byteIndex = 0; byteIndex < bytes.length; byteIndex++) {
                    bytes[byteIndex] = hammingCodePairToByte(codes[byteIndex * CODES_PER_BYTE],
                            codes[byteIndex * CODES_PER_BYTE + 1]);
                }
                payloadHandler.handle(bytes);
            } else {
                payloadHandler.handle(null);
            }
            bitIndex = 0;
            for (int i = 0; i < buffer.length; i++) {
                buffer[i] = null;
            }
        }
    }

    private boolean validBuffer() {
        int payloadIndex = 0;
        boolean nullPayload = false;
        while (payloadIndex < buffer.length) {
            if (buffer[payloadIndex] == null) {
                if (nullPayload) {
                    return false;
                } else {
                    buffer[payloadIndex] = new byte[packetLength];
                    nullPayload = true;
                }
            }
            payloadIndex++;
        }
        return true;
    }

    @Override
    public int getPacketSizeChange(int payloadSize) {
        return payloadSize * CODES_PER_BYTE / Byte.SIZE;
    }

    @Override
    public int getTimeout(int timeout) {
        return timeout;
    }
}
