package Voip.CRC;

import Voip.Payload.Layer;
import Voip.Payload.PayloadHandler;

import java.io.IOException;

import static Voip.CRC.HammingCode.*;

public class HammingEncoder implements Layer {

    private final int numPackets;

    public HammingEncoder() {
        this.numPackets = 1;
    }

    public static void main(String[] args) throws IOException {
        byte[] payload = new byte[]{1, 2, 3, 4, 5, 6, 7, 8};
        System.out.println(new HammingEncoder().getPacketSizeChange(payload.length));
        new HammingEncoder().processPayload(payload, new PayloadHandler() {
            @Override
            public void handle(byte[] payload) throws IOException {
                System.out.println("Packet bytes:");
                printBytes(payload);
            }
        });
    }

    @Override
    public void processPayload(byte[] payload, PayloadHandler payloadHandler) throws IOException {
        if (payload != null) {
            HammingCode[] codes = new HammingCode[payload.length * CODES_PER_BYTE];
            for (int byteIndex = 0; byteIndex < payload.length; byteIndex++) {
                for (int codeIndex = 0; codeIndex < CODES_PER_BYTE; codeIndex++) {
                    codes[byteIndex * CODES_PER_BYTE + codeIndex] = new HammingCode(
                            getBits(payload[byteIndex], codeIndex * DATA_SIZE,
                                    codeIndex * DATA_SIZE + 1,
                                    codeIndex * DATA_SIZE + 2,
                                    codeIndex * DATA_SIZE + 3));
                }
            }
//            for(HammingCode code : codes) {
//                printCode(code);
//                System.out.println();
//            }
            for (int codeBitIndex = 0; codeBitIndex < HammingCode.SIZE; codeBitIndex++) {
                byte[] bytes = new byte[codes.length / Byte.SIZE];
                for (int byteIndex = 0; byteIndex < bytes.length; byteIndex++) {
                    for (int byteBitIndex = 0; byteBitIndex < Byte.SIZE; byteBitIndex++) {
                        bytes[byteIndex] = setBit(bytes[byteIndex], byteBitIndex,
                                codes[byteIndex * Byte.SIZE + byteBitIndex].getBit(codeBitIndex));
                    }
                }
                payloadHandler.handle(bytes);
            }
        }
    }

    @Override
    public int getPacketSizeChange(int payloadSize) {
        return payloadSize * CODES_PER_BYTE / Byte.SIZE;
    }

    @Override
    public int getTimeout(int timeout) {
        return timeout;
    }
}
