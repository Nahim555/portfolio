package Voip.CRC;

import Voip.Payload.Layer;
import Voip.Payload.PayloadHandler;

import java.io.IOException;

import static Voip.CRC.HammingCode.*;

public class CrcEncoder implements Layer {

    public static final byte POLYNOMIAL = 29;

    public static byte crc8(byte[] bytes) {
        byte crc = 0;
        int bitIndex = 0;
        while (bitIndex < bytes.length * Byte.SIZE) {
            crc = crcStep(crc, getBit(bytes[bitIndex / Byte.SIZE], bitIndex % Byte.SIZE));
            bitIndex++;
        }
        // TODO last 8 bits
        for (int i = 0; i < Byte.SIZE; i++) {
            crc = crcStep(crc, false);
        }
        return crc;
    }

    private static byte crcStep(byte crc, boolean nextBit) {
        boolean msb = getBit(crc, 0);
        crc = (byte) (crc << 1);
        crc = setBit(crc, Byte.SIZE - 1, nextBit);
//        printByte(crc);
//        System.out.println();
        if (msb) {
            crc = (byte) (crc ^ POLYNOMIAL);
//            printByte(crc);
//            System.out.println();
        }
        return crc;
    }

    public static void main(String[] args) throws IOException {
        printByte(crc8(new byte[]{-62}));
        System.out.println();
        printByte(crc8(new byte[]{-63}));
        System.out.println();
        System.out.println();
        new CrcEncoder().processPayload(new byte[]{-62}, new PayloadHandler() {
            @Override
            public void handle(byte[] payload) throws IOException {
                printBytes(payload);
            }
        });
    }

    @Override
    public int getPacketSizeChange(int payloadSize) {
        return payloadSize + 1;
    }

    @Override
    public int getTimeout(int timeout) {
        return timeout;
    }

    @Override
    public void processPayload(byte[] payload, PayloadHandler payloadHandler) throws IOException {
        if (payload == null) {
            payloadHandler.handle(null);
        } else {
            byte[] temp = new byte[payload.length + 1];
            System.arraycopy(payload, 0, temp, 1, payload.length);
            temp[0] = crc8(payload);
            payload = temp;
            payloadHandler.handle(payload);
        }
    }
}
