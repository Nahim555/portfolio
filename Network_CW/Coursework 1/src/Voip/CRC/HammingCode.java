package Voip.CRC;

public class HammingCode {

    public static final int SIZE = 7;
    public static final int DATA_SIZE = 4;
    public static final int PARITY_SIZE = 3;
    public static final int CODES_PER_BYTE = Byte.SIZE / DATA_SIZE;
    private final boolean[] code = new boolean[SIZE];

    public HammingCode(boolean d0, boolean d1, boolean d2, boolean d3) {
        setDataBits(d0, d1, d2, d3);
    }

    public HammingCode(boolean p0, boolean p1, boolean d0, boolean p2, boolean d1, boolean d2, boolean d3) {
        setBits(p0, p1, d0, p2, d1, d2, d3);
    }

    // set a bit in a byte
    public static byte setBit(byte b, int position, boolean one) {
        position = Byte.SIZE - position - 1;
        checkBitPosition(position);
        if (one) {
            return (byte) (b | (1 << position));
        } else {
            return (byte) (b & ~(1 << position));
        }
    }

    public HammingCode(boolean[] bits) {
        if (bits.length == DATA_SIZE) {
            setDataBits(bits);
        } else if (bits.length == SIZE) {
            setBits(bits);
        } else {
            throw new IllegalArgumentException("Array incorrect size: " + bits.length);
        }
    }

    public static boolean oddParity(boolean... bits) {
        boolean parity = true;
        for (boolean bit : bits) {
            if (bit) {
                parity = !parity;
            }
        }
        return parity;
    }

    // get bit from byte
    public static boolean getBit(byte b, int position) {
        position = Byte.SIZE - 1 - position;
        checkBitPosition(position);
        return ((b >> position) & 1) == 1;
    }

    // convert byte to bit array
    public static boolean[] getBits(byte b, int... positions) {
        boolean[] bits = new boolean[positions.length];
        for (int i = 0; i < positions.length; i++) {
            bits[i] = getBit(b, positions[i]);
        }
        return bits;
    }

    public static void printCode(HammingCode code) {
        for (int i = 0; i < code.code.length; i++) {
            if (code.code[BIT_POSITION.values()[i].position]) {
                System.out.print("1");
            } else {
                System.out.print("0");
            }
        }
    }

    public static void printByte(byte b) {
        for (int i = 0; i < Byte.SIZE; i++) {
            if (getBit(b, i)) {
                System.out.print(1);
            } else {
                System.out.print(0);
            }
        }
    }

    public static void printBytes(byte[] bytes) {
        for (byte b : bytes) {
            printByte(b);
            System.out.println();
        }
    }

    public static void main(String[] args) {
        byte b = 0;
        byte c = 2;
        printByte(c);
        System.out.println();
        System.out.println(getBit(c, 6));
        printByte(setBit(b, 0, true));
    }

    // 1 byte to 2 hamming codes
    private static HammingCode[] byteToHammingCodePair(byte b) {
        HammingCode[] codes = new HammingCode[Byte.SIZE / DATA_SIZE];
        for (int i = 0; i < codes.length; i++) {
            codes[i] = new HammingCode(getBits(b, i * DATA_SIZE, i * DATA_SIZE + 1, i * DATA_SIZE + 2,
                    i * DATA_SIZE + 3));
        }
        return codes;
    }

    // validate the bit position accessed in a byte
    private static void checkBitPosition(int position) {
        if (position > Byte.SIZE - 1 || position < 0) {
            throw new IllegalArgumentException("Position out of range: " + position);
        }
    }

    // 2 hamming codes to 1 byte
    public static byte hammingCodePairToByte(HammingCode hammingCodeA, HammingCode hammingCodeB) {
        byte b = 0;
        boolean[] firstCodeBits = hammingCodeA.getDataBits();
        boolean[] secondCodeBits = hammingCodeB.getDataBits();
        for (int i = 0; i < DATA_SIZE; i++) {
            b = setBit(b, i, firstCodeBits[i]);
            b = setBit(b, i + DATA_SIZE, secondCodeBits[i]);
        }
        return b;
    }

    // 8 hamming codes to 1 byte representing column of hamming code set
    private static byte hammingCodeSetColumnToByte(HammingCode[] codes, int columnIndex) {
        if (codes.length != Byte.SIZE) {
            throw new IllegalArgumentException();
        }
        byte b = 0;
        for (int bitIndex = 0; bitIndex < Byte.SIZE; bitIndex++) {
            b = setBit(b, bitIndex, codes[bitIndex].getDataBit(columnIndex));
        }
        return b;
    }

    // 8 hamming codes to 7 bytes
    private static byte[] hammingCodeSetToBytes(HammingCode[] codes) {
        if (codes.length != Byte.SIZE) {
            throw new IllegalArgumentException();
        }
        byte[] bytes = new byte[HammingCode.SIZE];
        for (int byteIndex = 0; byteIndex < bytes.length; byteIndex++) {
            bytes[byteIndex] = hammingCodeSetColumnToByte(codes, byteIndex);
        }
        return bytes;
    }

    // 4 bytes to 8 hamming codes
    private static HammingCode[] byteSetToHammingCodes(byte[] bytes) {
        if (bytes.length != 4) {
            throw new IllegalArgumentException();
        }
        HammingCode[] codes = new HammingCode[8];
        for (int byteIndex = 0; byteIndex < 4; byteIndex++) {
            HammingCode[] pair = byteToHammingCodePair(bytes[byteIndex]);
            codes[byteIndex * 2] = pair[0];
            codes[byteIndex * 2 + 1] = pair[1];
        }
        return codes;
    }

    public boolean getBit(int position) {
        if (position < SIZE || position >= 0) {
            return code[position];
        } else {
            throw new IllegalArgumentException();
        }
    }

    public boolean getDataBit(int position) {
        return code[BIT_POSITION.values()[position].position];
    }

    private void setBits(boolean... bits) {
        System.arraycopy(bits, 0, code, 0, code.length);
        int k = 0;
        int change = 1;
        boolean[] parityBits = parityBits();
        for (int i = 0; i < PARITY_SIZE; i++) {
            if (parityBits[i] != code[BIT_POSITION.values()[i + DATA_SIZE].position]) {
                k += change;
            }
            change *= 2;
        }
        if (k > 0) {
            code[k - 1] = !code[k - 1];
        }
    }

    private boolean p0() {
        return oddParity(code[BIT_POSITION.D0.position], code[BIT_POSITION.D1.position], code[BIT_POSITION.D3.position]);
    }

    private boolean p1() {
        return oddParity(code[BIT_POSITION.D0.position], code[BIT_POSITION.D2.position], code[BIT_POSITION.D3.position]);
    }

    private boolean p2() {
        return oddParity(code[BIT_POSITION.D1.position], code[BIT_POSITION.D2.position], code[BIT_POSITION.D3.position]);
    }

    private boolean[] parityBits() {
        return new boolean[]{p0(), p1(), p2()};
    }

    public boolean[] getDataBits() {
        boolean[] bits = new boolean[DATA_SIZE];
        for (int i = 0; i < bits.length; i++) {
            bits[i] = code[BIT_POSITION.values()[i].position];
        }
        return bits;
    }

    public static boolean[] bytesToBits(byte[] bytes) {
        boolean[] booleans = new boolean[bytes.length * Byte.SIZE];
        for (int byteIndex = 0; byteIndex < bytes.length; byteIndex++) {
            for (int bitIndex = 0; bitIndex < Byte.SIZE; bitIndex++) {
                booleans[byteIndex * Byte.SIZE + bitIndex] = getBit(bytes[byteIndex], bitIndex);
            }
        }
        return booleans;
    }

    private void setDataBits(boolean... bits) {
        for (int i = 0; i < DATA_SIZE; i++) {
            code[BIT_POSITION.values()[i].position] = bits[i];
        }
        boolean[] parityBits = parityBits();
        for (int i = 0; i < PARITY_SIZE; i++) {
            code[BIT_POSITION.values()[i + DATA_SIZE].position] = parityBits[i];
        }
    }

    private enum BIT_POSITION {
        D0(2),
        D1(4),
        D2(5),
        D3(6),
        P0(0),
        P1(1),
        P2(3);

        public final int position;

        BIT_POSITION(int position) {
            this.position = position;
        }
    }
}
