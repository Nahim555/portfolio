package Voip.Sequencing;

import Voip.Payload.PayloadHandler;

import java.io.IOException;

public class Desequencer extends Sequenced {
    private final byte[][] buffer;
    private final int bufferOffset;
    private int index = -1;

    public Desequencer(int bufferOffset, int bufferSize) {
        if (bufferOffset < 0 && bufferSize < 1 && bufferOffset < bufferSize) {
            throw new IllegalArgumentException();
        }
        this.bufferOffset = bufferOffset;
        this.buffer = new byte[bufferSize][];
    }

    @Override
    public void processPayload(byte[] payload, PayloadHandler payloadHandler) throws IOException {
        if (index < 0) {
            // first ever packet
            if (payload == null) {
                payloadHandler.handle(null);
            } else {
                int sequenceNumber = addToBuffer(payload);
                index = sequenceNumber - bufferOffset;
                if (index < 0) {
                    index = buffer.length + index;
                }
                payloadHandler.handle(buffer[index]);
                buffer[index] = null;
            }
        } else {
            // buffer already populated, simply add to it
            if (payload != null) {
                addToBuffer(payload);
            }
            index = (index + 1) % buffer.length;
            payloadHandler.handle(buffer[index]);
            buffer[index] = null;
        }
    }

    @Override
    public int getTimeout(int timeout) {
        return timeout;
    }

    private int getSequenceNumber(byte[] payload) {
        return payload[0];
    }

    private int addToBuffer(byte[] payload) {
        int index = getSequenceNumber(payload);
        buffer[index] = new byte[payload.length - 1];
        System.arraycopy(payload, 1, buffer[index], 0, buffer[index].length);
//        System.out.print("Received: " + index);
        return index;
    }
}
