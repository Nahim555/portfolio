package Voip.Sequencing;

import Voip.Payload.PayloadHandler;

import java.io.IOException;

public class InterleaveSequencer extends Sequencer {
    private final int dimension;
    private final int numBatches;
    private byte index = 0;
    private byte[][] primaryBlock;
    private byte[][] secondaryBlock;

    public InterleaveSequencer(int dimension) {
        super((byte) (dimension * dimension));
        if (dimension > 11) {
            throw new IllegalArgumentException();
        }
        this.numBatches = 2;
        this.dimension = dimension;
        primaryBlock = new byte[dimension * dimension][];
        secondaryBlock = new byte[dimension * dimension][];
    }

    public static byte rotateIndex(byte origIndex, int dimension) {
        byte index = (byte) (origIndex % (dimension * dimension));
        byte rotatedIndex = rotateLimitedIndex(index, dimension);
        if (origIndex >= dimension * dimension) {
            return (byte) (rotatedIndex + dimension * dimension);
        } else {
            return rotatedIndex;
        }
    }

    public static byte rotateLimitedIndex(byte index, int dimension) {
        index = (byte) (index % (dimension * dimension));
        return (byte) ((index % dimension) * dimension + dimension - 1 - index / dimension);
    }

    @Override
    public void processPayload(byte[] payload, PayloadHandler payloadHandler) throws IOException {
        if (payload != null) {
            byte[] temp = payload;
            payload = new byte[temp.length + 1];
            System.arraycopy(temp, 0, payload, 1, temp.length);
            payload[0] = index;
//            System.out.println("Interleaved: " + index);
        } else {
//            System.out.println("Interleaved: null");
        }
        secondaryBlock[dimension * dimension - rotateLimitedIndex(index, dimension) - 1] = payload;
        byte[] output = primaryBlock[index % secondaryBlock.length];
//        if(index % 9 <= 7 && index % 9 >= 5) {
//            output = null;
//            System.out.println("No send");
//        }
        index++;
        if (index % secondaryBlock.length == 0) {
            primaryBlock = secondaryBlock;
            secondaryBlock = new byte[primaryBlock.length][];
            if (index == secondaryBlock.length * numBatches) {
                index = 0;
            }
        }
        payloadHandler.handle(output);
    }

    public static void main(String[] args) {
        for (int i = 0; i < 18; i++) {
            System.out.println(rotateIndex((byte) i, 3));
        }
    }
}
