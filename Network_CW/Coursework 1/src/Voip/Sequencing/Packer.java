package Voip.Sequencing;

import Voip.Payload.Layer;
import Voip.Payload.PayloadHandler;

import java.io.IOException;

public class Packer implements Layer {

    private final byte[][] buffer;
    private int index = 0;

    public Packer(int pack) {
        buffer = new byte[pack][];
    }

    @Override
    public void processPayload(byte[] payload, PayloadHandler payloadHandler) throws IOException {
        buffer[index] = payload;
        index++;
        if (index == buffer.length) {
            payload = new byte[buffer[0].length * buffer.length];
            for (int i = 0; i < buffer.length; i++) {
                System.arraycopy(buffer[i], 0, payload, i * buffer[i].length, buffer[i].length);
            }
            payloadHandler.handle(payload);
            index = 0;
        }
    }

    @Override
    public int getPacketSizeChange(int payloadSize) {
        return payloadSize * buffer.length;
    }

    @Override
    public int getTimeout(int timeout) {
        return timeout;
    }
}
