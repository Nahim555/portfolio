package Voip.Sequencing;

import Voip.Payload.Layer;
import Voip.Payload.PayloadHandler;

import java.io.IOException;

public class Unsplitter implements Layer {

    private final int split;

    public Unsplitter(int split) {
        if (split % 2 != 0) {
            throw new IllegalArgumentException();
        }
        this.split = split;
    }

    @Override
    public void processPayload(byte[] payload, PayloadHandler payloadHandler) throws IOException {
        payloadHandler.handle(payload);
    }

    @Override
    public int getPacketSizeChange(int payloadSize) {
        return payloadSize / split;
    }

    @Override
    public int getTimeout(int timeout) {
        return timeout / split;
    }
}
