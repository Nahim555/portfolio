package Voip.Sequencing;

import Voip.Payload.Layer;

public abstract class Sequenced implements Layer {

    @Override
    public int getPacketSizeChange(int payloadSize) {
        return payloadSize + 1;
    }
}
