package Voip.Sequencing;

import Voip.Payload.Layer;
import Voip.Payload.PayloadHandler;

import java.io.IOException;

public class Unpacker implements Layer {

    private final int unpack;

    public Unpacker(int unpack) {
        this.unpack = unpack;
    }

    @Override
    public void processPayload(byte[] payload, PayloadHandler payloadHandler) throws IOException {
        payloadHandler.handle(payload);
    }

    @Override
    public int getPacketSizeChange(int payloadSize) {
        return payloadSize * unpack;
    }

    @Override
    public int getTimeout(int timeout) {
        return timeout * unpack;
    }
}
