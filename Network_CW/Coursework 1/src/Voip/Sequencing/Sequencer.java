package Voip.Sequencing;

import Voip.Payload.PayloadHandler;

import java.io.IOException;

public class Sequencer extends Sequenced {

    private final byte limit;
    private byte sequenceNumber = 0;

    public Sequencer(byte limit) {
        this.limit = limit;
    }

    @Override
    public void processPayload(byte[] payload, PayloadHandler payloadHandler) throws IOException {
        if (payload != null) {
            byte[] sequencedPayload = new byte[payload.length + 1];
            System.arraycopy(payload, 0, sequencedPayload, 1, payload.length);
            sequencedPayload[0] = sequenceNumber;
            payloadHandler.handle(sequencedPayload);
        } else {
            payloadHandler.handle(payload);
        }
        sequenceNumber = (byte) ((sequenceNumber + 1) % limit);
    }

    @Override
    public int getTimeout(int timeout) {
        return timeout;
    }
}
