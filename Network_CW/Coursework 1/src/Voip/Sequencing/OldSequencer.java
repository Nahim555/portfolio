package Voip.Sequencing;

import Voip.Payload.PayloadHandler;

import java.io.IOException;

public class OldSequencer extends Sequenced {
    protected byte sequenceNumber = Byte.MIN_VALUE;

    @Override
    public void processPayload(byte[] payload, PayloadHandler payloadHandler) throws IOException {
        if (payload != null) {
            byte[] sequencedPayload = new byte[payload.length + 1];
            System.arraycopy(payload, 0, sequencedPayload, 1, payload.length);
            sequencedPayload[0] = sequenceNumber;
            payloadHandler.handle(sequencedPayload);
        } else {
            payloadHandler.handle(null);
        }
        sequenceNumber++;
    }

    @Override
    public int getTimeout(int timeout) {
        return timeout;
    }
}
