package Voip.Sequencing;

import Voip.Payload.PayloadHandler;

import java.io.IOException;

import static Voip.Sequencing.InterleaveSequencer.rotateIndex;

public class InterleaveDesequencer extends Sequenced {
    private final byte[][] buffer;
    private final int bufferOffset;
    private final int dimension;
    private int index = -1;

    public InterleaveDesequencer(int dimension) {
        this.bufferOffset = dimension * dimension;
        this.dimension = dimension;
        this.buffer = new byte[dimension * dimension * 2][];
    }

    @Override
    public void processPayload(byte[] payload, PayloadHandler payloadHandler) throws IOException {
        if (index < 0) {
            // first ever packet
            if (payload == null) {
                payloadHandler.handle(null);
            } else {
                byte sequenceNumber = addToBuffer(payload);
                index = rotateIndex(rotateIndex(rotateIndex(sequenceNumber, dimension), dimension), dimension) - bufferOffset;
                if (index < 0) {
                    index = buffer.length + index;
                }
                payloadHandler.handle(buffer[index]);
                buffer[index] = null;
            }
        } else {
            // buffer already populated, simply add to it
            if (payload != null) {
                addToBuffer(payload);
//                index = rotateIndex(rotateIndex(rotateIndex(addToBuffer(payload), dimension), dimension), dimension) - bufferOffset;
//                if (index < 0) {
//                    index = buffer.length + index;
//                }
            }
            index = (index + 1) % buffer.length;
//            else {
//                index = (index + 1) % buffer.length;
//            }
            payloadHandler.handle(buffer[index]);
            buffer[index] = null;
        }
    }

    @Override
    public int getTimeout(int timeout) {
        return timeout;
    }

    private byte getSequenceNumber(byte[] payload) {
        return payload[0];
    }

    private byte addToBuffer(byte[] payload) {
        byte index = getSequenceNumber(payload);
        buffer[index] = new byte[payload.length - 1];
        System.arraycopy(payload, 1, buffer[index], 0, buffer[index].length);
//        System.out.print("Received: " + index);
        return index;
    }
}
