package Voip.Sequencing;

import Voip.Payload.Layer;
import Voip.Payload.PayloadHandler;

import java.io.IOException;

public class Splitter implements Layer {

    private final int split;

    public Splitter(int split) {
        if (split % 2 != 0) {
            throw new IllegalArgumentException();
        }
        this.split = split;
    }

    @Override
    public void processPayload(byte[] payload, PayloadHandler payloadHandler) throws IOException {
        for (int i = 0; i < split; i++) {
            byte[] bytes = new byte[payload.length / split];
            System.arraycopy(payload, i * bytes.length, bytes, 0, bytes.length);
            payloadHandler.handle(bytes);
        }
    }

    @Override
    public int getPacketSizeChange(int payloadSize) {
        return payloadSize / split;
    }

    @Override
    public int getTimeout(int timeout) {
        return timeout / split;
    }
}
