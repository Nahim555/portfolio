package Analysis;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class Sender extends ThreadedStopper implements Notifier {

    public final static int FLUSH = 100000;
    private final static String EXTENSION = ".txt";
    private final long startingSequenceNumber;
    private final InetAddress address;
    private final DatagramSocket socket;
    private final int port;
    private final int dummyPort;
    private final Receiver receiver;
    private final File logFile;
    private final String tag;
    private long sequenceNumber = -1;

    public Sender(Analyser.SocketObtainer obtainer, InetAddress address, int port, int dummyPort) throws IOException {
        this.address = address;
        this.port = port;
        this.dummyPort = dummyPort;
        tag = obtainer.getId();
        this.socket = obtainer.getSocket();
        this.logFile = new File(Analyser.ANALYSIS_DIRECTORY, obtainer.getId() + EXTENSION);
        if (logFile.exists()) {
            String lastLine = getLastLine(logFile);
            if (lastLine == null || lastLine.isEmpty()) {
                // Empty file / line
                startingSequenceNumber = -1;
                logFile.delete();
            } else {
                startingSequenceNumber = Integer.parseInt(lastLine);
                removeLastLine(logFile);
            }
        } else {
            startingSequenceNumber = -1;
        }
        receiver = new Receiver(obtainer.getSocket(port), logFile, this, tag);
    }

    private String getLastLine(File file) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(file));
        String previousLine = null;
        String currentLine = null;
        while ((currentLine = reader.readLine()) != null) {
            previousLine = currentLine;
        }
        return previousLine;
    }

    private void removeLastLine(File file) throws IOException {
        File tempFile = File.createTempFile("temp", null);
        BufferedReader fileReader = new BufferedReader(new FileReader(file));
        BufferedWriter tempWriter = new BufferedWriter(new FileWriter(tempFile));
        String previousLine = fileReader.readLine();
        String currentLine = fileReader.readLine();
        if (previousLine != null) {
            while (currentLine != null) {
                tempWriter.write(previousLine);
                tempWriter.newLine();
                previousLine = currentLine;
                currentLine = fileReader.readLine();
            }
        }
        tempWriter.write(previousLine);
        fileReader.close();
        tempWriter.close();
        BufferedReader tempReader = new BufferedReader(new FileReader(tempFile));
        BufferedWriter fileWriter = new BufferedWriter(new FileWriter(file));
        previousLine = tempReader.readLine();
        currentLine = tempReader.readLine();
        if (previousLine != null) {
            while (currentLine != null) {
                fileWriter.write(previousLine);
                fileWriter.newLine();
                previousLine = currentLine;
                currentLine = tempReader.readLine();
            }
        }
        fileWriter.close();
        tempReader.close();
    }

    @Override
    public void run() {
        Thread.currentThread().setPriority(Thread.MIN_PRIORITY);
        new Thread(receiver).start();
        try {
            sequenceNumber = startingSequenceNumber;
            while (!isStopped()) {
                send(++sequenceNumber);
            }
        } catch (IOException e) {
            e.printStackTrace();
            stop();
        }
        receiver.stop();
        log("Stopped sender");
    }

    private void send(long sequenceNumber) throws IOException {
        Packet packet = new Packet(sequenceNumber, System.nanoTime());
        DatagramPacket datagramPacket = new DatagramPacket(packet.pack(),
                Packet.getLength(), address, port);
//        log("Sending" + packet.toString());
        socket.send(datagramPacket);
    }

    private void sendDummy() throws IOException {
        socket.send(new DatagramPacket(new Packet(-1L, -1L).pack(), Packet.getLength(), address, dummyPort));
    }

    @Override
    void onStop() {
        log("Stopping sender");
    }

    @Override
    String getTag() {
        return tag;
    }

    @Override
    public void onFinish() {
        log("Receiver finished");
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(logFile, true));
            writer.write(String.valueOf(sequenceNumber));
            writer.close();
            log("Done");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
