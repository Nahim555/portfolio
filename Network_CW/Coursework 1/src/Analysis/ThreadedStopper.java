package Analysis;

public abstract class ThreadedStopper extends Stopper implements Runnable {

    public final Thread start() {
        Thread thread = new Thread(this);
        thread.start();
        return thread;
    }
}
