package Analysis;

public interface Notifier {
    void onFinish();
}
