package Analysis;

import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

public class Receiver extends ThreadedStopper implements Notifier {

    private final String tag;
    private final DatagramSocket socket;
    private final DatagramPacket datagramPacket = new DatagramPacket(Packet.getEmptyPayload(), Packet.getLength());
    private final LinkedBlockingQueue<Pair<Packet, Long>> packetQueue = new LinkedBlockingQueue<>(1000000);
    private final Writer writer;
    private final Notifier receiverComms;

    public Receiver(DatagramSocket socket, File logFile, Notifier comms, String id) throws SocketException {
        this.socket = socket;
        this.receiverComms = comms;
        tag = id;
        this.socket.setSoTimeout((int) TimeUnit.MILLISECONDS.convert(1, TimeUnit.SECONDS));
        writer = new Writer(packetQueue, logFile, this, id);
    }

    @Override
    public void onFinish() {
        log("Writer finished");
        receiverComms.onFinish();
    }

    @Override
    void onStop() {
        log("Stopping receiver");
    }

    @Override
    String getTag() {
        return tag;
    }

    @Override
    public void run() {
        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
        Thread writerThread = new Thread(writer);
        writerThread.start();
        while (!isStopped()) {
            try {
                receive();
            } catch (IOException e) {
                e.printStackTrace();
                stop();
            }
        }
        writer.stop();
        writerThread.interrupt();
        log("Stopped receiver");
    }

    private void receive() throws IOException {
        try {
            socket.receive(datagramPacket);
            long arrivalTime = System.nanoTime();
            Packet packet = new Packet(datagramPacket.getData());
//        log("Received " + packet.toString());
            boolean put = false;
            while (!put) {
                try {
                    packetQueue.put(new Pair<>(packet, arrivalTime));
                    put = true;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (SocketTimeoutException e) {
            // timeout
        }
    }

    public interface ReceiverComms {
        void onFinish();
    }
}
