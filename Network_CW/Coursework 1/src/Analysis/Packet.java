package Analysis;

import java.util.Arrays;
import java.util.zip.CRC32;

public class Packet {

    private final long sequenceNumber;
    private final long timestamp;
    private final long checksum;

    public Packet(byte[] payload) {
        sequenceNumber = byteArrayToLong(Arrays.copyOfRange(payload, 0, Long.BYTES));
        timestamp = byteArrayToLong(Arrays.copyOfRange(payload, Long.BYTES, 2 * Long.BYTES));
        checksum = byteArrayToLong(Arrays.copyOfRange(payload, Long.BYTES * 2, 3 * Long.BYTES));
    }

    public Packet(long sequenceNumber, long timestamp) {
        this.sequenceNumber = sequenceNumber;
        this.timestamp = timestamp;
        checksum = generateChecksum();
    }

    public static int getLength() {
        return 3 * Long.BYTES;
    }

    private static byte[] longToByteArray(long l) {
        byte[] result = new byte[Long.BYTES];
        for (int i = Byte.SIZE - 1; i >= 0; i--) {
            result[i] = (byte) (l & 0xFF);
            l >>= Byte.SIZE;
        }
        return result;
    }

    private static long byteArrayToLong(byte[] b) {
        long result = 0;
        for (int i = 0; i < Byte.SIZE; i++) {
            result <<= Byte.SIZE;
            result |= (b[i] & 0xFF);
        }
        return result;
    }

    public static byte[] getEmptyPayload() {
        return new byte[getLength()];
    }

    public long getSequenceNumber() {
        return sequenceNumber;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public byte[] pack() {
        return concatByteArray(concatByteArray(longToByteArray(sequenceNumber), longToByteArray(timestamp)),
                longToByteArray(checksum));
    }

    private byte[] concatByteArray(byte[] a, byte[] b) {
        byte[] result = new byte[a.length + b.length];
        System.arraycopy(a, 0, result, 0, a.length);
        System.arraycopy(b, 0, result, a.length, b.length);
        return result;
    }

    private long generateChecksum() {
        CRC32 checksum = new CRC32();
        checksum.update(longToByteArray(timestamp));
        checksum.update(longToByteArray(sequenceNumber));
        return checksum.getValue();
    }

    public boolean isCorrupt() {
        long localChecksum = generateChecksum();
        return localChecksum != checksum;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(sequenceNumber);
        builder.append(',');
        builder.append(timestamp);
        builder.append(',');
        builder.append(checksum);
        return builder.toString();
    }
}
