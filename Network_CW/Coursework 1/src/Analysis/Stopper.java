package Analysis;

import java.util.concurrent.atomic.AtomicBoolean;

public abstract class Stopper {
    private final AtomicBoolean stop = new AtomicBoolean(false);

    public boolean isStopped() {
        return stop.get();
    }

    public void stop() {
        stop.set(true);
        onStop();
    }

    abstract void onStop();

    void log(String msg) {
        System.out.println(getTag() + ": " + msg);
    }

    abstract String getTag();
}
