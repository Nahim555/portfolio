package Analysis;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;

public class Writer extends ThreadedStopper {

    private final static String CORRUPT = "corrupt";
    private final static String SEPERATOR = ",";
    private final LinkedBlockingQueue<Pair<Packet, Long>> packetQueue;
    private final File logFile;
    private final Notifier writerComms;
    private final String tag;

    public Writer(LinkedBlockingQueue<Pair<Packet, Long>> packetQueue, File logFile, Notifier comms, String id) {
        this.packetQueue = packetQueue;
        this.logFile = logFile;
        this.writerComms = comms;
        tag = id;
    }

    private String getLogString(Pair<Packet, Long> pair) {
        StringBuilder stringBuilder = new StringBuilder();
        Packet packet = pair.key;
        long arrivalTime = pair.value;
        if (packet.isCorrupt()) {
            stringBuilder.append(CORRUPT);
        } else {
            stringBuilder.append(packet.getSequenceNumber());
            stringBuilder.append(SEPERATOR);
            stringBuilder.append(arrivalTime - packet.getTimestamp());
        }
        String line = stringBuilder.toString();
//        log(line);
        return line;
    }

    private Pair<Packet, Long> getPair() throws InterruptedException {
        Pair<Packet, Long> pair = null;
        while (pair == null && !isStopped()) {
            try {
                pair = packetQueue.take();
            } catch (InterruptedException e) {
                throw new InterruptedException();
            }
        }
        return pair;
    }

    @Override
    public void run() {
        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(logFile, true));
            try {
                boolean stop = false;
                while (!isStopped()) {
                    bufferedWriter.write(getLogString(getPair()));
                    bufferedWriter.newLine();
                }
            } catch (InterruptedException e) {
                // Interrupted by receiver thread
            } finally {
                Pair<Packet, Long> pair = null;
                while ((pair = packetQueue.poll()) != null) {
                    bufferedWriter.write(getLogString(pair));
                    bufferedWriter.newLine();
                }
                bufferedWriter.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
            stop();
        }
        log("Stopped writer");
        writerComms.onFinish();
    }

    @Override
    void onStop() {
        log("Stopping writer");
    }

    @Override
    String getTag() {
        return tag;
    }
}
