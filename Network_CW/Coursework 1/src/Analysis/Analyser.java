package Analysis;

import java.io.File;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Set;
import java.util.TreeSet;

public class Analyser extends Stopper {

    public static final File ANALYSIS_DIRECTORY = new File("Analysis");
    private final static Set<Integer> OCCUPIED_PORTS = new TreeSet<>();

    static {
        ANALYSIS_DIRECTORY.mkdir();
    }

    private final String tag;
    private final Sender sender;

    public Analyser(SocketObtainer obtainer) throws IOException {
        tag = obtainer.getId();
        int port = getPort();
        int dummyPort = getPort();
        InetAddress address = InetAddress.getByName("localhost");
        sender = new Sender(obtainer, address, port, dummyPort);
        log("Starting analysis");
        sender.start();
    }

    @Override
    void onStop() {
        sender.stop();
    }

    @Override
    String getTag() {
        return tag;
    }

    private int getPort() {
        int port = 55555;
        boolean portFound = false;
        synchronized (OCCUPIED_PORTS) {
            while (!portFound) {
                if (OCCUPIED_PORTS.contains(port)) {
                    port++;
                } else {
                    portFound = true;
                }
            }
            OCCUPIED_PORTS.add(port);
        }
        log("Using port " + port);
        return port;
    }

    public interface SocketObtainer {
        String getId();

        DatagramSocket getSocket() throws SocketException;

        DatagramSocket getSocket(int port) throws SocketException;
    }
}
