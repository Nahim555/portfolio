import Voip.Configuration;
import Voip.Voip;

import javax.sound.sampled.LineUnavailableException;
import java.io.IOException;

public class CompA {
    public static void main(String[] args) throws IOException, LineUnavailableException {
        // Change below to channel option / ip of other computer / port to operate on
        new Voip(Configuration.DS1_DEFAULT, 192, 168, 137, 144, 55555);
    }
}
