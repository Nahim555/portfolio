function [delays, corruptBursts, corruptBurstDistances, outOfOrderDistances, outOfOrderBursts, outOfOrderBurstDistances, missingPacketBursts, missingPacketBurstDistances] = analyse(path, n)
    fid = fopen(path);
    line = fgetl(fid);
    nPackets = n;
    corruptString = "corrupt";
    delimiter = ",";
    corruptBursts = [];
    currentCorruptBurst = 0;
    corruptBurstDistances = [];
    currentSeqNumber = 0;
    largestSeqNumber = 0;
    index = 0;
    delays = [];
    previousSeqNumber = 0;
    missingPackets = [];
    outOfOrderDistances = [];
    outOfOrderBursts = [];
    outOfOrderBurstDistances = [];
    currentOutOfOrderBurst = 0;
    while ischar(line) && currentSeqNumber < nPackets
        if strcmp(corruptString, line)
            currentCorruptBurst = currentCorruptBurst + 1;
        else
            fields = strsplit(line, delimiter);
            delays = cat(1, delays, str2double(fields{2}));
            currentSeqNumber = str2double(fields{1});
            if currentCorruptBurst > 0
                corruptBurstDistances = [corruptBurstDistances, currentSeqNumber - largestSeqNumber];
                corruptBursts = [corruptBursts, currentCorruptBurst];
                currentCorruptBurst = 0;
            end
            if currentSeqNumber > largestSeqNumber || currentSeqNumber == 0
                if currentSeqNumber - largestSeqNumber > 1
                    for seq = largestSeqNumber + 1 : currentSeqNumber - 1
                        missingPackets = [missingPackets, seq];
                    end
                end
                if currentOutOfOrderBurst > 0
                    outOfOrderBurstDistances = [outOfOrderBurstDistances, currentSeqNumber - largestSeqNumber];
                    outOfOrderBursts = [outOfOrderBursts, currentOutOfOrderBurst];
                    currentOutOfOrderBurst = 0;
                end
                largestSeqNumber = currentSeqNumber;
            else
                if size(missingPackets, 2) == 1
                    missingPackets = [];
                else
                    index = 1;
                    found = false;
                    while index < size(missingPackets, 2) && ~found
                        if missingPackets(index) == currentSeqNumber
                            found = true;
                        else
                            index = index + 1; 
                        end
                    end
                    if index == 1
                        missingPackets = missingPackets(2:size(missingPackets, 2));
                    elseif index == size(missingPackets, 2)
                        missingPackets = missingPackets(1 : size(missingPackets, 2) - 1);
                    else 
                        missingPackets = [missingPackets(1 : index - 1), missingPackets(index + 1 : size(missingPackets, 2))];
                    end
                end
                outOfOrderDistances = [outOfOrderDistances, largestSeqNumber - currentSeqNumber + currentOutOfOrderBurst];
                currentOutOfOrderBurst = currentOutOfOrderBurst + 1;
            end
            previousSeqNumber = currentSeqNumber;
            largestSeqNumber
        end
        line = fgetl(fid);
    end
    if currentCorruptBurst > 0
        corruptBursts = [corruptBursts, currentCorruptBurst];
        currentCorruptBurst = 0;
    end
    if currentOutOfOrderBurst > 0
        outOfOrderBursts = [outOfOrderBursts, currentOutOfOrderBurst];
        currentOutOfOrderBurst = 0;
    end
    missingPacketBursts = [];
    missingPacketBurstDistances = [];
    if size(missingPackets, 2) > 0
        previous = missingPackets(1);
        currentMissingBurst = 1;
        for index = 2 : size(missingPackets, 2)
            if missingPackets(index) == previous + 1
                currentMissingBurst = currentMissingBurst + 1;
            else
                missingPacketBurstDistances = [missingPacketBurstDistances, missingPackets(index) - previous - 1];
                missingPacketBursts = [missingPacketBursts, currentMissingBurst];
                currentMissingBurst = 1;
            end
            previous = missingPackets(index);
        end
        missingPacketBursts = [missingPacketBursts, currentMissingBurst];
        currentMissingBurst = 1;
    end
    fclose(fid);
end

