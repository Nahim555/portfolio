function [] = draw(i1,i2,xl,yl,t)
    a = max([size(i1),size(i2)]);
    group = [    ones(a);
        2 * ones(a)];
    boxplot([i1; i2],group,'whisker',Inf);
    hold on;
%     plot(1,mean(i1),'-or','MarkerFaceColor', 'r');
%     plot(2,mean(i2),'-or','MarkerFaceColor', 'r');
%     plot(3,mean(i3),'-or','MarkerFaceColor', 'r');
%     plot(4,mean(i4),'-or','MarkerFaceColor', 'r');
    hold off;
    xlabel(xl);
    ylabel(yl);
    title(t);
end

