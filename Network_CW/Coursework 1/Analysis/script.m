x1 = a'; x2 = b';
x = [x1;x2];
g = [ones(size(x1)); 2*ones(size(x2))];
boxplot(x,g)
hold on;
plot(1,mean(x1),'-or','MarkerFaceColor', 'r');
plot(2,mean(x2),'-or','MarkerFaceColor', 'r');
hold off;