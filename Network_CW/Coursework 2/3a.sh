#! /bin/bash


run() {
	rm -r "$LOG_DIR"
	mkdir "$LOG_DIR"
	COMMAND="wget -O /dev/null --progress=bar:force:noscroll"
	for((i=0;i<NUM_DOWNLOADS;i++)); do
		printf '%s\n' "Downloading small file $i"
		$COMMAND $SMALL_FILE_URL -a $LOG_DIR/small
		printf '%s\n' "Downloading medium file $i"
		$COMMAND $MEDIUM_FILE_URL -a $LOG_DIR/medium
		printf '%s\n' "Downloading large file $i"
		$COMMAND $BIG_FILE_URL -a $LOG_DIR/large
		printf '%s\n' "Downloading huge file $i"
		$COMMAND $HUGE_FILE_URL -a $LOG_DIR/huge
	done
}

printHelp() {
	printf -- "-s\tSmall file URL\n"
	printf -- "-m\tMedium file URL\n"
	printf -- "-b\tBig file URL\n"
	printf -- "-h\tHuge file URL\n"
	printf -- "-l\tLog directory\n"
	printf -- "-n\tNumber of downloads of each file\n"
}

doGetopts() {
	if(("$#" != 12)); then
		printf "Wrong number of parameters!\n"
		printHelp
		exit 1;
	fi
	while getopts ":s:m:b:l:n:h:" option; do
		case $option in
			s)
				SMALL_FILE_URL="$OPTARG"
				;;
			m)
				MEDIUM_FILE_URL="$OPTARG"
				;;
			b)
				BIG_FILE_URL="$OPTARG"
				;;
			h)
				HUGE_FILE_URL="$OPTARG"
				;;
			l)
				LOG_DIR="$OPTARG"
				;;
			n)
				NUM_DOWNLOADS="$OPTARG"
				;;
			\?)
				printf "Invalid option: %s\n" "$OPTARG"
				printHelp
				exit 1
				;;
			:)
				printf "Option %s requires an argument\n" "$OPTARG"
				printHelp
				exit 1
				;;
		esac
	done
	run
}

doGetopts "$@"