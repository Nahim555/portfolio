distinctRtts = [];
index = 1;
while index < size(rtts,2)
    add = true;
    stop = false;
    if size(distinctRtts,2) > 0
        distIndex = 1;
        while ~stop
            if distIndex == size(distinctRtts,2) + 1
                stop = true;
            else
                if distinctRtts(distIndex) == rtts(index)
                    add = false;
                    stop = true;
                end 
            end
            distIndex = distIndex + 1;
        end
    end
    if add
        distinctRtts = cat(2, distinctRtts, rtts(index));
    end
    index = index + 1;
end

distinctRtts = sort(distinctRtts,2);

dist = zeros(1,size(distinctRtts,2));
index = 1;
while index < size(rtts,2)
    distIndex = 1;
    stop = false;
    while ~stop
        if distIndex == size(dist,2) + 1
            error('This shouldnt happen');
            stop=true;
        else
            if distinctRtts(distIndex) == rtts(index)
                stop = true;
                dist(distIndex) = dist(distIndex) + 1;
            end
        end
        distIndex = distIndex + 1;
    end
    index = index + 1;
end

plot(distinctRtts(1,(2:size(distinctRtts,2))),dist(1,(2:size(dist,2))))
title('RTT Distribution')
ylabel('Frequency')
xlabel('RTT (ms)')