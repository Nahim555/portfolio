#! /bin/bash

run() {
	rm -r "$LOG_DIR"
	mkdir "$LOG_DIR"
	for((i=1;i<=K;i++)); do
		printf '%s\n' "Downloading $i files simulatenously"
		bash ./3cSub.sh -u "$URL" -k $i -l "$LOG_DIR/$i"
	done
}

printHelp() {
	printf -- "-u\tURL of file to download\n"
	printf -- "-k\tNumber of simulatenous downloads\n"
	printf -- "-l\tLog directory to write to\n"
}

doGetopts() {
	if(("$#" != 6)); then
		printf "Wrong number of parameters!\n"
		printHelp
		exit 1;
	fi
	while getopts ":u:k:l:" option; do
		case $option in	
			u)
				URL="$OPTARG"
				#printf "ADDRESS=%s\n" "$ADDRESS"
				;;
			k)
				K="$OPTARG"
				#printf "NUM_PINGS=%s\n" "$NUM_PINGS"
				;;
			l)
				LOG_DIR="$OPTARG"
				#printf "LOG_FILE=%s\n" "$LOG_FILE"
				;;
			\?)
				printf "Invalid option: %s\n" "$OPTARG"
				printHelp
				exit 1
				;;
			:)
				printf "Option %s requires an argument\n" "$OPTARG"
				printHelp
				exit 1
				;;
		esac
	done
	run
}

doGetopts "$@"