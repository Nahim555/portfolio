#! /bin/bash

run() {
	rm -r $LOG_DIR
	mkdir $LOG_DIR
	i=0
	while read LINE; do
		if(($i==0)); then
			TITLE=$LINE
			i=1;
		else
			i=0;
			wget -O /dev/null --progress=bar:force -a $LOG_DIR/$TITLE $LINE
		fi		
	done < "$BIG_FILE_URL"
}

printHelp() {
	printf -- "-b\tFTPs file URL\n"
	printf -- "-l\tLog directory\n"
}

doGetopts() {
	if(("$#" != 4)); then
		printf "Wrong number of parameters!\n"
		printHelp
		exit 1;
	fi
	while getopts ":f:l:" option; do
		case $option in
			f)
				BIG_FILE_URL="$OPTARG"
				;;
			l)
				LOG_DIR="$OPTARG"
				;;
			\?)
				printf "Invalid option: %s\n" "$OPTARG"
				printHelp
				exit 1
				;;
			:)
				printf "Option %s requires an argument\n" "$OPTARG"
				printHelp
				exit 1
				;;
		esac
	done
	run
}

doGetopts "$@"