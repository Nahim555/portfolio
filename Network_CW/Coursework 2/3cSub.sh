#! /bin/bash

run() {
	COMMAND="wget -O /dev/null $URL -a $LOG_FILE --progress=bar:force:noscroll"
	for((i=0;i<K;i++)); do
		printf '%s\n' "Simulatenous operation $(($i+1))"
		$($COMMAND) &
	done
	wait
}

printHelp() {
	printf -- "-u\tURL of file to download\n"
	printf -- "-k\tNumber of simulatenous downloads\n"
	printf -- "-l\tLog file to write to\n"
}

doGetopts() {
	if(("$#" != 6)); then
		printf "Wrong number of parameters!\n"
		printHelp
		exit 1;
	fi
	while getopts ":u:k:l:" option; do
		case $option in	
			u)
				URL="$OPTARG"
				#printf "ADDRESS=%s\n" "$ADDRESS"
				;;
			k)
				K="$OPTARG"
				#printf "NUM_PINGS=%s\n" "$NUM_PINGS"
				;;
			l)
				LOG_FILE="$OPTARG"
				#printf "LOG_FILE=%s\n" "$LOG_FILE"
				;;
			\?)
				printf "Invalid option: %s\n" "$OPTARG"
				printHelp
				exit 1
				;;
			:)
				printf "Option %s requires an argument\n" "$OPTARG"
				printHelp
				exit 1
				;;
		esac
	done
	run
}

doGetopts "$@"