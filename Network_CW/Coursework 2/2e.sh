#! /bin/bash

run() {
	COMMAND="ping -c $NUM_PINGS $ADDRESS"
	printf "Executing: %s\n" "$COMMAND"
	OUTPUT=$($COMMAND)
	printf '%s\n' "$OUTPUT"
	printf "Logging output to %s\n" "$LOG_FILE"
	echo "$OUTPUT" >> "$LOG_FILE"
	echo "" >> "$LOG_FILE"
}

printHelp() {
	printf -- "-a\tAddress to ping\n"
	printf -- "-s\tNumber of seconds to ping address\n"
	printf -- "-l\tLog file to write to\n"
}

doGetopts() {
	if(("$#" != 6)); then
		printf "Wrong number of parameters!\n"
		printHelp
		exit 1;
	fi
	while getopts ":a:s:l:" option; do
		case $option in	
			a)
				ADDRESS="$OPTARG"
				#printf "ADDRESS=%s\n" "$ADDRESS"
				;;
			s)
				NUM_PINGS="$OPTARG"
				#printf "NUM_PINGS=%s\n" "$NUM_PINGS"
				;;
			l)
				LOG_FILE="$OPTARG"
				#printf "LOG_FILE=%s\n" "$LOG_FILE"
				;;
			\?)
				printf "Invalid option: %s\n" "$OPTARG"
				printHelp
				exit 1
				;;
			:)
				printf "Option %s requires an argument\n" "$OPTARG"
				printHelp
				exit 1
				;;
		esac
	done
	run
}

doGetopts "$@"