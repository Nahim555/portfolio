fid = fopen('results/2e2');
line = fgetl(fid);
rtts = zeros(1,5000);
a = [];
c = [];
results = [];
index = 1;
while ischar(line)
    [sStartIndex] = regexp(line, 'icmp_seq=\d+');
    if ~isempty(sStartIndex)
        c = str2double(c(10:size(c,2)));
        [startIndex,endIndex] = regexp(line, '\d+\.?\d+\sms');
        a = line(startIndex:endIndex);
        if size(a, 2) ~= 0
            a = str2double(a(1:size(a,2)-3));
%             rtts(c) = a;
%             rtts = cat(1,rtts,a);
            rtts(index)=a;
        end
    end
    index = index + 1;
    line = fgetl(fid);
end
fclose(fid);

x = (0:4999);
previousIndex = 1;
index = 1;
hold on;
while index <= 5000
    if rtts(index) == 0
        plot((previousIndex:index-1),rtts(previousIndex:index-1));
        previousIndex = index+1;
    end
    index = index + 1;
end
if index < 5000
    plot((previousIndex:index),rtts(previousIndex:index));    
end
index = 1;
while index < 5000
    if rtts(index) == 0
        plot([index,index],[20,55]);
    end
    index = index + 1;
end
hold off;