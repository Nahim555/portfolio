#! /bin/bash

run() {
	mkdir "$LOG_DIR"
	while read LINE; do
		printf '\n%s\n' "Running traceroute for $LINE"
		(date >> $LOG_DIR/$LINE && traceroute -n $LINE >> $LOG_DIR/$LINE && echo "" >> $LOG_DIR/$LINE) &
	done < "$WEBSITES"
	wait
}

printHelp() {
	printf -- "-w\tWebsites to ping\n"
	printf -- "-l\tLog directory to write to\n"
}

doGetopts() {
	if(("$#" != 4)); then
		printf "Wrong number of parameters!\n"
		printHelp
		exit 1;
	fi
	while getopts ":w:l:s:" option; do
		case $option in	
			w)
				WEBSITES="$OPTARG"
				#printf "ADDRESS=%s\n" "$ADDRESS"
				;;
			l)
				LOG_DIR="$OPTARG"
				#printf "LOG_DIR=%s\n" "$LOG_DIR"
				;;
			\?)
				printf "Invalid option: %s\n" "$OPTARG"
				printHelp
				exit 1
				;;
			:)
				printf "Option %s requires an argument\n" "$OPTARG"
				printHelp
				exit 1
				;;
		esac
	done
	run
}

doGetopts "$@"