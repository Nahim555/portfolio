function [] = makeRTTLossGraph(a, b)
    x = (0:27);
    yyaxis left
    plot(x,a(:,2));
    set(gca,'xtick',(0:4:28));
    set(gca,'xticklabel',{'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun'});
    xlabel('Time (days)');
    ylabel('Median RTT (ms)');
    yyaxis right
    plot(x,a(:,5));
    ylabel('Packet Loss (%)');
    title(['RTT vs Packet Loss for ', b]);
end