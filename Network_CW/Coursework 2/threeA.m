fid = fopen('results/3d3/pingLog');
line = fgetl(fid);
rtts = [];
a = [];
while ischar(line)
    [startIndex,endIndex] = regexp(line, '\d+\.\d+\sms');
    b = a;
    a = line(startIndex:endIndex);
    if size(b, 2) ~= 0
        b = str2double(b(1:size(b,2)-3));
        rtts = cat(2, rtts, b); 
    end
    line = fgetl(fid);
end
fclose(fid);