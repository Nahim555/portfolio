hold on;
a = 1;
index = 1;
while index <= size(rtts,2)
    if rtts(index) == 0
        plot([a:index-1],rtts(a:index-1));
        a = index + 1;
    end
    index = index + 1;
end
plot([a:index], rtts(a:index));
hold off;