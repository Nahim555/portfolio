x=(0:6:6*27)-7+12;
plot(x,a,x,b,x,c);
set(gca,'xtick',(0:24:24*7));
set(gca,'xticklabel',{'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun'});
xlabel('Time (days)');
ylabel('Route')
set(gca,'ytick',(0:2))
set(gca,'yticklabel',{'Oklahoma', 'San Francisco', 'Wichita'});
title('Routes');
legend('Traceroute 1','Traceroute 2','Traceroute 3','location','northwest')