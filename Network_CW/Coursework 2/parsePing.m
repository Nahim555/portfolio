function [ results ] = parsePing( filepath )
    fid = fopen(filepath);
    line = fgetl(fid);
    rtts = [];
    results = [];
    while ischar(line)
        [destUStart] = regexpi(line, 'Destination Port Unreachable');
        if isempty(destUStart) ~= 0
            [doneStart] = regexp(line, 'min/avg/max/mdev');
            if ~isempty(doneStart)
                loss = 100 - size(rtts, 1) / 120 * 100;
                results = cat(1,results,[mean(rtts), median(rtts), min(rtts), max(rtts), loss]);
                rtts = [];
            else
                [sStartIndex,sEndIndex] = regexp(line, 'icmp_seq=\d+');
                c = line(sStartIndex:sEndIndex);
                if size(c,2) ~= 0
                    c = str2double(c(10:size(c,2)));
                    [startIndex,endIndex] = regexp(line, '\d+\.?\d+\sms');
                    a = line(startIndex:endIndex);
                    if size(a, 2) ~= 0
                        a = str2double(a(1:size(a,2)-3));
                        rtts = cat(1,rtts,a);
                    end
                end
            end
        end
        line = fgetl(fid);
    end
    fclose(fid);
end

