#! /bin/bash

trap "kill 0" SIGINT

run() {
	rm -r "$LOG_DIR"
	mkdir "$LOG_DIR"
	LOG_FILE="$LOG_DIR/wgetLog"
	LOG_PING="$LOG_DIR/pingLog"
	COMMAND="wget -O /dev/null $URL -a $LOG_FILE --progress=bar:force:noscroll"
	# (ping $PING_URL > $LOG_PING) &
	# printf '%s\n' "Doing intial sleep"
	# sleep "$(($TIME))s"
	for((i=0;i<K;i++)); do
		printf '%s\n' "Downloading then sleeping $i"
		for((j=0;j<$SIM;j++)) {
			printf '%s\n' "Sim $j"
			$($COMMAND)	&
		}
		wait
		sleep "$(($TIME))s"
	done
	#kill -TERM -$$
}

printHelp() {
	printf -- "-u\tURL of file to download\n"
	printf -- "-k\tNumber of downloads to conduct\n"
	printf -- "-l\tLog directory\n"
	printf -- "-q\tPing url\n"
	printf -- "-t\tTime (in seconds) delay between downloads\n"
}

doGetopts() {
	if(("$#" != 12)); then
		printf "Wrong number of parameters!\n"
		printHelp
		exit 1;
	fi
	while getopts ":u:k:l:t:q:s:" option; do
		case $option in
			q)
				PING_URL="$OPTARG"
				#printf "PING_URL=%s\n" "$PING_URL"
				;;
			u)
				URL="$OPTARG"
				#printf "ADDRESS=%s\n" "$ADDRESS"
				;;
			s)
				SIM="$OPTARG"
				#printf "ADDRESS=%s\n" "$ADDRESS"
				;;
			k)
				K="$OPTARG"
				#printf "NUM_PINGS=%s\n" "$NUM_PINGS"
				;;
			l)
				LOG_DIR="$OPTARG"
				#printf "LOG_FILE=%s\n" "$LOG_FILE"
				;;
			t)
				TIME="$OPTARG"
				#printf "TIME=%s\n" "$TIME"
				;;
			\?)
				printf "Invalid option: %s\n" "$OPTARG"
				printHelp
				exit 1
				;;
			:)
				printf "Option %s requires an argument\n" "$OPTARG"
				printHelp
				exit 1
				;;
		esac
	done
	run
}

doGetopts "$@"