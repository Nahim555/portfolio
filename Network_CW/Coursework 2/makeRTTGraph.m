function [] = makeRTTGraph(a, b)
    x = (0:27);
    plot(x,a);
    plot(x,a(:,1:4));
    set(gca,'xtick',(0:4:28));
    set(gca,'xticklabel',{'Mon', 'Tue', 'Wed', 'Thur', 'Fri', 'Sat', 'Sun'});
    xlabel('Time (days)');
    ylabel('RTT (ms)');
    title(['RTTs for ', b]);
    legend('Mean', 'Median', 'Min', 'Max', 'location', 'south');
end

