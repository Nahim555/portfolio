package Throughput;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

public class Throughput {
    public static void main(String[] args) throws IOException {
        String pattern = ".+\\d+\\.\\d+ MB/s.+";
        File dir = new File("results/3b");
        for (File file : dir.listFiles()) {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            List<String> lines = new LinkedList<>();
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.matches(pattern)) {
                    lines.add(line);
                    System.out.println(line);
                }
            }
            System.out.println("Size: " + lines.size());
            BufferedWriter writer = new BufferedWriter(new FileWriter(file));
            for (String str : lines) {
                writer.write(str);
                writer.newLine();
            }
        }
    }
}
