package Traceroute;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class TracerouteRun {

    private final List<TracerouteRunEntry> entries = new ArrayList<>();
    private final Date date;
    private final String title;
    private final String destinationIp;

    public TracerouteRun(List<String> lines) {
        date = parseDate(lines.remove(0));
        title = lines.remove(0);
        for (String line : lines) {
            entries.add(new TracerouteRunEntry(line));
        }
        String[] titleParts = title.split("[\\\\(|\\\\)]");
        destinationIp = titleParts[1];
    }

    //reads the Date from first line.
    public static Date parseDate(String str) {
        DateFormat formatter = new SimpleDateFormat("EEE dd MMM kk:mm:ss zzz YYYY");
        try {
            Date date = formatter.parse(str);
            return date;
        } catch (ParseException e) {
            e.printStackTrace();
            System.exit(1);
        }
        return null;
    }

    public static void main(String[] args) {
        String str = "Mon 19 Mar 23:59:59 GMT 2018";
        parseDate(str);
    }

    public List<Route> getRoutes() {
        List<Route> routes = new ArrayList<>();
        for (Node node : entries.get(0).getNodes()) {
            routes.add(new Route(date));
        }
        for (TracerouteRunEntry entry : entries) {
            for (int nodeIndex = 0; nodeIndex < routes.size(); nodeIndex++) {
                routes.get(nodeIndex).addNode(entry.getNodes().get(nodeIndex));
            }
        }
        for (Route route : routes) {
            route.addNode(new Node(destinationIp, -1));
        }
        return routes;
    }

    public Set<Route> getDistinctRoutes() {
        Set<Route> distinctRoutes = new HashSet<>();
        List<Route> routes = getRoutes();
        for (Route route : routes) {
            distinctRoutes.add(route);
        }
        return distinctRoutes;
    }
}
