package Traceroute;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class LogParser {

    public static List<Route> parse(String logFilename) throws IOException {
        File logFile = new File(logFilename);
        if (!logFile.exists()) {
            throw new IllegalArgumentException("Log file does not exist");
        }
        List<TracerouteRun> runs = new ArrayList<>();
        BufferedReader reader = new BufferedReader(new FileReader(logFile));
        List<String> lines = new ArrayList<String>();
        String line;
        while ((line = reader.readLine()) != null) {
            if (line.isEmpty()) {
                // traceroute run done
                runs.add(new TracerouteRun(lines));
                lines.clear();
            } else {
                lines.add(line);
            }
        }
        List<Route> routes = new ArrayList<>();
        for (TracerouteRun run : runs) {
            routes.addAll(run.getDistinctRoutes());
        }
        return routes;
    }

    public static List<Route> parseDistinct(String logFilename) throws IOException {
        return new ArrayList<>(new TreeSet<>(parse(logFilename)));
    }
}
