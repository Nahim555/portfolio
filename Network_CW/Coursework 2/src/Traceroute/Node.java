package Traceroute;

public class Node implements Comparable<Node> {
    private final String ip;
    private final double time;

    public Node(String ip, double time) {
        this.ip = ip;
        this.time = time;
    }

    public String getIp() {
        return ip;
    }

    public double getTime() {
        return time;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        if (ip != null) {
            stringBuilder.append(ip);
            stringBuilder.append(" ");
        }
        if (time < 0) {
            stringBuilder.append("*");
        } else {
            stringBuilder.append(time);
            stringBuilder.append(" ms");
        }
        return stringBuilder.toString();
    }

    @Override
    public int compareTo(Node node) {
        if (ip == null) {
            if (node.ip == null) {
                return 0;
            } else {
                return -1;
            }
        } else {
            if (node.ip == null) {
                return 1;
            } else {
                String a = stripIp(ip);
                String b = stripIp(node.ip);
                return a.compareTo(b);
            }
        }
    }

    private String stripIp(String ip) {
        return ip;// ip.substring(0, ip.substring(0, ip.lastIndexOf(".")).lastIndexOf("."));
    }

    @Override
    public int hashCode() {
        if (ip == null) {
            return -1;
        } else {
            byte[] bytes = ip.getBytes();
            int hash = 1;
            for (byte b : bytes) {
                hash *= b;
            }
            return hash;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Node)) {
            return false;
        } else {
            return compareTo((Node) o) == 0;
        }
    }
}
