package Traceroute;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Route implements Comparable<Route> {

    private List<Node> nodes = new ArrayList<>();
    private Date date;

    public Route(Date date) {
        this.date = date;
    }

    public void addNode(Node aNode) {
        nodes.add(aNode);
    }

    public List<Node> getNodes() {
        return nodes;
    }

    public Date getDate() {
        return date;
    }

    public String toString() {
        StringBuilder st = new StringBuilder();
        st.append(date);
        st.append("\n");
        for (Node node : nodes) {
            st.append(node);
            st.append("\n");
        }
        return st.toString();
    }

    @Override
    public int compareTo(Route route) {
        if(route.nodes.size() > nodes.size()) {
            return 1;
        }
        else if(route.nodes.size() == nodes.size()) {
            for(int i = 0; i < nodes.size(); i++) {
                int result = nodes.get(i).compareTo(route.nodes.get(i));
                if(result != 0) {
                    return result;
                }
            }
            return 0;
        }
        else {
            return -1;
        }
    }

    @Override
    public int hashCode() {
        int hash = 1;
        for (Node node : nodes) {
            hash *= node.hashCode();
        }
        return hash;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Route)) {
            return false;
        } else {
            return compareTo((Route) o) == 0;
        }
    }
}
