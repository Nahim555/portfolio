package Traceroute;

import java.util.ArrayList;
import java.util.List;

public class TracerouteRunEntry {
    private static final String IP_PATTERN_STRING = "(\\d+\\.\\d+\\.\\d+\\.\\d+)";
    private static final String STAR_PATTERN_STRING = "\\*";
    private static final String ERROR_PATTERN_STRING = "!\\S";
    private static final String MS_PATTERN_STRING = "ms";

    private final int index;
    private final List<Node> nodes;

    public TracerouteRunEntry(String line) {
        line = line.trim();
        String[] parts = line.split("\\s");
        index = Integer.parseInt(parts[0]);
        String ip = null;
        nodes = new ArrayList<>();
        for (int partIndex = 1; partIndex < parts.length; partIndex++) {
            String str = parts[partIndex];
            if (!str.isEmpty()) {
                if (str.matches(IP_PATTERN_STRING)) {
                    ip = str;
                } else if (!str.matches(MS_PATTERN_STRING) && !str.matches(ERROR_PATTERN_STRING)) {
                    double time = -1;
                    if (!str.matches(STAR_PATTERN_STRING)) {
                        time = Double.parseDouble(str);
                    }
                    nodes.add(new Node(ip, time));
                }
            }
        }
    }

    public static void main(String[] args) {
        String line = "10  89.149.139.6  14.206 ms * 89.149.139.5  *";
        TracerouteRunEntry routeEntry = new TracerouteRunEntry(line);
        System.out.println(routeEntry);
    }

    public List<Node> getNodes() {
        return nodes;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(index);
        for (Node node : nodes) {
            stringBuilder.append(" ");
            stringBuilder.append(node);
        }
        return stringBuilder.toString();
    }
}
