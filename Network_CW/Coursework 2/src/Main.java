import Traceroute.LogParser;
import Traceroute.Node;
import Traceroute.Route;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

public class Main {

//    public static void main(String[] args) throws IOException {
//        String src = "results/1a/withhusbandintow.com";
//        List<Route> routes = LogParser.parse(src);
//        List<Route> distinctRoutes = LogParser.parseDistinct(src);
//        System.out.println(distinctRoutes.size());
//        System.out.println();
//        BufferedWriter writer = new BufferedWriter(new FileWriter(new File("w")));
//        for(Route route : routes) {
//            writer.write(String.valueOf(distinctRoutes.indexOf(route)));
//            writer.newLine();
//            System.out.println(distinctRoutes.indexOf(route));
//        }
//        writer.close();
//    }

    public static void main(String[] args) throws IOException {
        for (File file : new File("results/1a").listFiles(new FilenameFilter() {
            @Override
            public boolean accept(File file, String s) {
                return !s.contains(".html");
            }
        })) {
            System.out.println("Writing " + file.toString());
            BufferedWriter writer = new BufferedWriter(new FileWriter(
                    new File(file.getPath() + ".html")));
            writer.write(routesToScript(LogParser.parse(file.getPath())));
            writer.write(getContent());
            writer.close();
        }
    }

//    public static void main(String[] args) throws IOException {
//        File file = new File("tr");
//        BufferedWriter writer = new BufferedWriter(new FileWriter(
//                new File(file.getPath() + ".html")));
//        writer.write(routesToScript(LogParser.parse(file.getPath())));
//        writer.write(getContent());
//        writer.close();
//        System.out.println("Wrote " + file.toString());
//    }

    private static void addRoute(Route route, StringBuilder stringBuilder) {
        stringBuilder.append("[");
        List<Node> nodes = route.getNodes();
        String ip = null;
        String previousIp = null;
        int nodeIndex = 0;
        boolean added = false;
        while (nodeIndex < nodes.size()) {
            previousIp = ip;
            ip = nodes.get(nodeIndex).getIp();
            nodeIndex++;
            if (previousIp != null) {
                if (added) {
                    stringBuilder.append(",");
                }
                stringBuilder.append("'");
                stringBuilder.append(previousIp);
                stringBuilder.append("'");
                added = true;
            }
        }
        stringBuilder.append("]");
    }

    private static String getContent() throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(new File("map/index.html")));
        StringBuilder sb = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            sb.append(line);
            sb.append("\n");
        }
        return sb.toString();
    }

    public static String routesToScript(List<Route> routes) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("<script>var ips=[");
        Route route = null;
        Route previous = null;
        boolean added = false;
        int routeIndex = 0;
        while (routeIndex < routes.size()) {
            previous = route;
            route = routes.get(routeIndex);
            routeIndex++;
            if (added) {
                stringBuilder.append(",");
            }
            added = true;
            addRoute(route, stringBuilder);
        }
        stringBuilder.append("];</script>");
        return stringBuilder.toString();
    }
}
